#ifndef GET_NEXT_LINE
# define GET_NEXT_LINE

#include "../libft/libft.h"
# include <stdlib.h>
# include <stdio.h>
# include <fcntl.h>
# include <unistd.h>
# include <string.h>

#define BUFF_SIZE 1
#define SUCCESS 1
#define ERROR 0
#define FD_MAX 1000

typedef struct s_get_buffer
{
	int fd;
	int ret;
}              t_get_buffer;

int     get_next_line(const int fd, char **line);
void    ft_read(t_get_buffer *ptr, char **buffer, char **line);
void    printf_2d_buffer(char **buffer);
int     check_buffer(t_get_buffer *ptr, char **buffer, char **line);
void    manage_buffer(char **buffer, size_t i);
void    manage_line(int is_join, char **line, char **buffer, size_t i);


#endif

