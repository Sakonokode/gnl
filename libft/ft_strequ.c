/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 13:16:56 by maattal           #+#    #+#             */
/*   Updated: 2016/11/17 15:05:04 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strequ(char const *s1, char const *s2)
{
	if (s1 == NULL && s2 == NULL)
		return (1);
	if (s1 && s2)
	{
		if (ft_strlen((char *)s1) == ft_strlen((char *)s2))
		{
			if (ft_strstr((char *)s1, (char *)s2))
				return (1);
		}
	}
	return (0);
}
