/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 11:10:27 by maattal           #+#    #+#             */
/*   Updated: 2016/11/30 10:22:23 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	char	*ptr0;
	char	*ptr1;
	size_t	buffer;

	ptr0 = (char *)dst;
	ptr1 = (char *)src;
	buffer = 0;
	while (buffer < n)
	{
		ptr0[buffer] = ptr1[buffer];
		buffer++;
	}
	return (dst);
}
