#include "../libft/libft.h"
#include "../includes/get_next_line.h"

void	manage_buffer(char **buffer, size_t i)
{
	char *old_buffer;

	old_buffer = *buffer;
	if ((*buffer)[i + 1])
	{
		*buffer = ft_strsub(*buffer, i + 1, ft_strlen(*buffer));
		ft_strdel(&old_buffer);
	}
	else
		ft_strdel(buffer);
}

void	manage_line(int is_join, char **line, char **buffer, size_t i)
{
	char *old_line;
	char *eol;

	if (is_join)
	{
		old_line = *line;
		eol = ft_strsub(*buffer, 0, i);
		*line = ft_strjoin(*line, eol);
		ft_strdel(&old_line);
		ft_strdel(&eol);
	}
	else
		*line = ft_strdup("");
	manage_buffer(buffer, i);
}

int		check_buffer(t_get_buffer *ptr, char **buffer, char **line)
{
	size_t	i;
	char	*tmp;

	i = -1;
	ptr->ret = 1;
	while (*buffer && ++i < ft_strlen(*buffer))
	{
		if ((*buffer)[i] == '\n')
		{
			if (i == 0 && *line && ft_strlen(*line) == 0)
				manage_line(0, line, buffer, i);
			else
				manage_line(1, line, buffer, i);
			return (1);
		}
	}
	tmp = *line;
	*line = ft_strjoin(*line, *buffer);
	if (*line == NULL)
		*line = ft_strdup(*buffer);
	ft_strdel(&tmp);
	ft_strdel(buffer);
	return (0);
}

void	ft_read(t_get_buffer *ptr, char **buffer, char **line)
{
	char *buf;

	buf = ft_strnew(BUFF_SIZE);
	if (buf == NULL)
	{
		ptr->ret = -1;
		return ;
	}
	while ((ptr->ret = read(ptr->fd, buf, BUFF_SIZE)))
	{
		buf[ptr->ret] = '\0';
		*buffer = ft_strjoin(*buffer, buf);
		if (*buffer == NULL)
			*buffer = ft_strdup(buf);
		if ((ptr->ret < 0) || check_buffer(ptr, buffer, line))
			break ;
	}
	ft_strdel(&buf);
}

int		get_next_line(int const fd, char **line)
{
	t_get_buffer		*ptr;
	static char	**buffer = NULL;

	if (fd < 0 || fd >= FD_MAX || BUFF_SIZE <= 0 || !line)
		return (-1);
	if (!(ptr = (t_get_buffer *)malloc(sizeof(t_get_buffer))))
		return (-1);
	ptr->fd = fd;
	if (buffer == NULL)
		if (!(buffer = (char **)malloc(sizeof(char *) * FD_MAX)))
			return (-1);
	*line = ft_strnew(0);
	if (buffer[fd] == NULL || !check_buffer(ptr, &buffer[fd], line))
		ft_read(ptr, &buffer[fd], line);
	if (ptr->ret == -1)
		return (-1);
	if (ptr->ret == 0 && buffer[fd] == NULL && *line && ft_strlen(*line) == 0)
	{
		ft_strdel(line);
		ft_memdel((void *)&ptr);
		return (0);
	}
	ft_memdel((void *)&ptr);
	return (1);
}
